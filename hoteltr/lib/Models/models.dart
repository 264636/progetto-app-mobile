import 'dart:convert';
import 'dart:typed_data';

import 'package:flutter/cupertino.dart';
import 'package:sqflite/sqflite.dart';
import 'package:crypto/crypto.dart';

  class Hotel{
    String Nome;
    String Localita;
    DateTime? Apertura;
    DateTime? Chiusura;
    String Servizi;
    String Descrizione;
    String Email;
    int Stanze;
    Uint8List Image; // campo per contenere l'immagine come sequenza di byte

  Hotel(
      {
        required this.Nome,
        required this.Localita,
        required this.Apertura,
        required this.Chiusura,
        required this.Servizi,
        required this.Descrizione,
        required this.Email,
        required this.Stanze,
        required this.Image
      }
      );
  static Future<void> createTable(Database db,int versione) async{
    await db.execute(
        '''
       CREATE TABLE Hotel(
       Nome TEXT PRIMARY KEY,
       Localita TEXT,
       Apertura TEXT,
       Chiusura TEXT,
       Servizi TEXT,
       Descrizione TEXT,
       Email TEXT,
       Stanze INTEGER,
       Image BLOB
       )
       '''
    );
  }
  Map<String, dynamic> toMap(){
    return {
      'Nome':Nome,
      'Localita':Localita,
      'Apertura':Apertura.toString(),
      'Chiusura':Chiusura.toString(),
      'Servizi' : Servizi,
      'Descrizione':Descrizione,
      'Email':Email,
      'Stanze':Stanze,
      'Image':Image,
    };
  }

  factory Hotel.fromMap(Map<String, dynamic> map){
    return Hotel(
        Nome: map['Nome'],
        Localita: map['Localita'],
        Apertura: DateTime(DateTime.parse(map['Apertura']).year,DateTime.parse(map['Apertura']).month,DateTime.parse(map['Apertura']).day),
        Chiusura: DateTime(DateTime.parse(map['Chiusura']).year,DateTime.parse(map['Chiusura']).month,DateTime.parse(map['Chiusura']).day),
        Servizi: map['Servizi'],
        Descrizione: map['Descrizione'],
        Email: map['Email'],
        Stanze: map['Stanze'],
        Image: map['Image']
    );
  }
}
class Stanze{
  int IdStanza;
  String Servizi;
  int Nposti;
  int Prezzo;
  Uint8List Image;

  Stanze({
    required this.IdStanza,
    required this.Servizi,
    required this.Nposti,
    required this.Prezzo,
    required this.Image,
  });

  static Future<void> createTable(Database db,int version) async{
    await db.execute(
        '''
      CREATE TABLE Stanze(
      IdStanza INTEGER PRIMARY KEY,
      Servizi TEXT,
      Nposti INTEGER,
      Prezzo INTEGER,
      Image BLOB
      )
      '''
    );
  }

  Map<String, dynamic> toMap(){
    return{
      'IdStanza':IdStanza,
      'Servizi':Servizi,
      'Nposti':Nposti,
      'Prezzo':Prezzo,
      'Image':Image
    };
  }

  factory Stanze.fromMap(Map<String, dynamic> map){
    return Stanze(
        IdStanza: map['IdStanza'],
        Servizi: map['Servizi'],
        Nposti: map['Nposti'],
        Prezzo: map['Prezzo'],
        Image: map['Image']
    );
  }
}

class Prenotazioni{
  int IdPrenotazione;
  DateTime DataInizio;
  DateTime DataFine;
  int IdStanza;
  String Username;
  int Sconto;
  int Stato;

  Prenotazioni(
      {
        required this.IdPrenotazione,
        required this.DataInizio,
        required this.DataFine,
        required this.IdStanza,
        required this.Username,
        required this.Sconto,
        required this.Stato
      }
      );

  static Future<void> createTable(Database db,int version) async{
    await db.execute('''
    CREATE TABLE Prenotazioni(
    IdPrenotazione INTEGER PRIMARY KEY,
    DataInizio TEXT,
    DataFine TEXT,
    IdStanza INTEGER,
    Username TEXT,
    Sconto INTEGER,
    Stato INTEGER,
    FOREIGN KEY (IdStanza) REFERENCES Stanze(IdStanza),
    FOREIGN KEY(Username) REFERENCES Utenti(Username)
    )
    ''');
  }

  Map<String,dynamic> toMap(){
    return{
      'IdPrenotazione':IdPrenotazione,
      'DataInizio' :DataInizio.toString(),
      'DataFine':DataFine.toString(),
      'IdStanza':IdStanza,
      'Username':Username,
      'Sconto':Sconto,
      'Stato':Stato
    };
  }

  factory Prenotazioni.fromMap(Map<String,dynamic> map){
    return Prenotazioni(
        IdPrenotazione: map['IdPrenotazione'],
        DataInizio: DateTime(DateTime.parse(map['DataInizio']).year,DateTime.parse(map['DataInizio']).month,DateTime.parse(map['DataInizio']).day),
        DataFine: DateTime(DateTime.parse(map['DataFine']).year,DateTime.parse(map['DataFine']).month,DateTime.parse(map['DataFine']).day),
        IdStanza: map['IdStanza'],
        Username: map['Username'],
        Sconto: map['Sconto'],
        Stato: map['Stato']
    );
  }
}
class Utenti{
  String Username;
  String Password;
  int Sconto;

  Utenti(
      {
        required this.Username,
        required this.Password,
        required this.Sconto
      }
      );

  static Future<void> createTable(Database db,int version) async{
    await db.execute('''
    CREATE TABLE Utenti(
    Username TEXT PRIMARY KEY,
    Password TEXT,
    Sconto INTEGER
    )
    ''');
  }

  Map<String,dynamic> toMap() {
    return {
      'Username': Username,
      'Password': Password,
      'Sconto': Sconto,
    };
  }

  factory Utenti.fromMap(Map<String,dynamic> map){
    return Utenti(
        Username: map['Username'],
        Password: map['Password'],
        Sconto: map['Sconto']
    );
  }
}

String generateMd5(String input) {
  return md5.convert(utf8.encode(input)).toString();
}

String generateSha256(String input) {
  return sha256.convert(utf8.encode(input)).toString();
}