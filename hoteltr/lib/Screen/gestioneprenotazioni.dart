import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:hoteltr/Widget/base.dart';
import 'package:provider/provider.dart';

import '../DatabaseHelper/DataBaseHelper.dart';
import '../Models/models.dart';
import '../Sessioni.dart';
import '../Widget/card.dart';

class gestioneprenotazioni extends StatefulWidget{
  late Utenti? utente;
  gestioneprenotazioni({Key? key,this.utente});
  @override
  _gestioneprenotazioni createState()=> _gestioneprenotazioni();
}

class _gestioneprenotazioni extends State<gestioneprenotazioni>{
  late String Messaggio;
  late List<Prenotazioni> prenotazioni=[];
  List<Stanze> stanze=[];
  DatabaseHelper dbhelper = DatabaseHelper();

  void loadprenotazioni() async{
    prenotazioni= await dbhelper.getPrenotazioni();
    setState(() {});
  }


  void loadstanze() async{
    stanze= await dbhelper.getStanze();
    setState(() {});
  }

  void cambiastato(Prenotazioni pr,bool stato) async{
    if(stato){
      pr.Stato=1;
      dbhelper.CambiaPrenotazione(pr);
      setState(() {});
    }
    else{
      DateTime startTime = DateTime.now();
      dbhelper.RimuoviPrenotazione(pr);
      loadprenotazioni();
      DateTime endTime = DateTime.now(); // Fine del calcolo del tempo
      double executionTime = endTime.difference(startTime).inMilliseconds / 1000.0 ; // Calcola il tempo trascorso in secondi come numero decimale
      print('Tempo di esecuzione per eliminazione record: $executionTime millisecondi');
    }
  }

  String StampaStato(int stato) {
    if(stato == 0){
      return "Non accettata";
    }
    else{
      return "Accettata";
    }
  }

  @override
  void initState(){
    loadstanze();
    super.initState();
  }

  void aggiorna(){
    setState(() {
     loadprenotazioni();
    });
  }
  Widget build(BuildContext context) {

    final userSession = Provider.of<UserSession>(context,listen:true);

    void loadprenotazioniutente() async{
      prenotazioni= await dbhelper.getPrenotazioniUtente(userSession.username);
      setState((){});
    }

    if(userSession.isAdmin() ){
      loadprenotazioni();
    }else{
      loadprenotazioniutente();
    }

    Stanze? stanza(Prenotazioni pr) {
      for (Stanze st in stanze) {
        if (pr.IdStanza == st.IdStanza) {
          return st;
        }
      }
    }
    return customscreen(messaggio: "Prenotazioni ",
        body:Container(color: Colors.orange[100], child:CustomScrollView(
          physics: AlwaysScrollableScrollPhysics(),
          slivers: <Widget>[
            SliverFillRemaining(
              child: ListView.builder(
                itemCount: prenotazioni.length,
                itemBuilder: (BuildContext context,int index){
                  return ListTile(
                      title: CardPrenotazioni(stanza: stanza(prenotazioni[index])!,aggpr: aggiorna,prenotazione: prenotazioni[index],cambiastato: cambiastato,)
                  );
                },
              ),
            ),
          ],
        ),
    )
    );
  }
}