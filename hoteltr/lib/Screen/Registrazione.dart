import 'package:flutter/material.dart';
import 'package:hoteltr/Widget/base.dart';
import 'package:provider/provider.dart';
import 'package:hoteltr/Screen/Pannelloctr.dart';
import '../DatabaseHelper/DataBaseHelper.dart';
import '../Models/models.dart';
import '../Sessioni.dart';

class Registrazione extends StatefulWidget {
  Registrazione({Key? key}) : super(key: key);

  @override
  _Registrazione createState() => _Registrazione();
}

  class _Registrazione extends State<Registrazione> {
  late  int nutenti=1;
  DatabaseHelper dbhelper=DatabaseHelper();
  TextEditingController Usernamectr = TextEditingController();
  TextEditingController Passwordctr = TextEditingController();

  @override
  Widget build(BuildContext context) {
    final userSession = Provider.of<UserSession>(context);
    return customscreen(messaggio: "Home",
        body: Card(
          child: Column(
            children: <Widget>[
              Padding(
                padding: EdgeInsets.only(top: 20),
                child: Text("Registrati",style: TextStyle(color: Colors.black),),
              ),
              TextFormField(
                controller: Usernamectr,
                maxLength: 10,
                decoration: InputDecoration(
                  labelStyle: TextStyle(color:Colors.black),
                  focusedBorder: OutlineInputBorder(
                      borderSide: BorderSide(color: Colors.black)
                  ),
                  labelText: "Username",
                  icon:Icon(Icons.people,color: Colors.blueGrey,),
                ),
              ),
              TextFormField(
                controller: Passwordctr,
                obscureText: true,
                maxLength: 10,
                decoration: InputDecoration(
                    labelStyle: TextStyle(color:Colors.black),
                    focusedBorder: OutlineInputBorder(
                        borderSide: BorderSide(color: Colors.black)
                    ),
                    labelText: "Password",
                    icon: Icon(Icons.password,color: Colors.blueGrey,)
                ),
              ),
              ElevatedButton(onPressed: (){
                nuovoutente(Usernamectr.text,Passwordctr.text);
              }, child: Text("Registrati"))
            ],
          ),
        )
    );
  }

  void nuovoutente(String username,String password) async{
    nutenti = nutenti+1;
    Utenti utente=Utenti(Username: username, Password: generateMd5(password), Sconto: 20);
    await dbhelper.InsertUtente(utente);
    Navigator.pop(context);
  }
}