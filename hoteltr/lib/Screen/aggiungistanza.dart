import 'dart:typed_data';

import 'package:flutter/material.dart';
import 'package:hoteltr/Widget/base.dart';
import 'package:hoteltr/main.dart';
import 'package:hoteltr/Models/models.dart';
import 'package:provider/provider.dart';
import '../DatabaseHelper/DataBaseHelper.dart';
import '../Sessioni.dart';
import 'package:image_picker/image_picker.dart';
import 'dart:io';



class aggiungistanza extends StatefulWidget{
  List<Stanze> stanze;
  final Function newstanza;
  late Hotel? hotel;
  aggiungistanza({Key? key,required this.hotel,required this.newstanza,required this.stanze}) : super(key:key);

  @override
  _aggiungistanza createState() => _aggiungistanza();
}

class _aggiungistanza extends State<aggiungistanza> {
  DatabaseHelper dbhelper = DatabaseHelper();
  late Uint8List immagine;

  TextEditingController Servizi = TextEditingController();
  TextEditingController Nposti = TextEditingController();
  TextEditingController Prezzzo = TextEditingController();

  Future<void> _selectImage() async {
    final picker = ImagePicker();
    final pickedFile = await picker.pickImage(source: ImageSource.gallery);
    if (pickedFile != null) {
      final bytes = await pickedFile.readAsBytes();
      setState(() {
        immagine = bytes;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    final hotelSession = Provider.of<HotelSession>(context);
    // TODO: implement build
    return customscreen(
      body: Card(
          child:Column(
            children: <Widget>[
              TextFormField(
                controller: Servizi,
                decoration: const InputDecoration(
                  labelStyle: TextStyle(color: Colors.black),
                  focusedBorder: OutlineInputBorder(
                    borderSide: BorderSide(color: Colors.black),
                  ),
                  labelText: "Servizi",
                  icon: Icon(Icons.wifi, color: Colors.blueGrey),
                ),
              ),
              TextFormField(
                controller: Nposti,
                decoration: const InputDecoration(
                  labelStyle: TextStyle(color: Colors.black),
                  focusedBorder: OutlineInputBorder(
                    borderSide: BorderSide(color: Colors.black),
                  ),
                  labelText: "Nposti",
                  icon: Icon(Icons.numbers, color: Colors.blueGrey),
                ),
              ),
              TextFormField(
                controller: Prezzzo,
                decoration: const InputDecoration(
                  labelStyle: TextStyle(color: Colors.black),
                  focusedBorder: OutlineInputBorder(
                    borderSide: BorderSide(color: Colors.black),
                  ),
                  labelText: "Prezzo per notte",
                  icon: Icon(Icons.euro, color: Colors.blueGrey),
                ),
              ),
              ElevatedButton(
                onPressed: () {
                  _selectImage();
                },
                child: Text("Seleziona un'immagine"),
              ),
              ElevatedButton(
                onPressed: () {
                  nuovastanza(
                      Servizi.text,
                      int.parse(Nposti.text),
                      int.parse(Prezzzo.text),
                      immagine);
                },
                child: const Text("Registra la tua stanza"),
              ),
            ],
          )
      ),
      messaggio: 'Aggiungi stanza',
    );
  }
  bool limstanza(int? nstanzelibere,Idnuovastanza){
    if(nstanzelibere != null) {
      if (Idnuovastanza < nstanzelibere) {
        return true;
      }
      return false;
    }
    else{
      return true;
    }
  }
  void nuovastanza(String Servizi,int Nposti,int Prezzo,Uint8List immagine) async{
    int cont=widget.stanze.length;
    Stanze newstanza;
    if(limstanza(widget.hotel?.Stanze, cont)) {
      newstanza = Stanze(IdStanza: cont+1, Servizi: Servizi, Nposti: Nposti, Prezzo: Prezzo, Image: immagine);
      await dbhelper.InsertStanza(newstanza);
      widget.newstanza(newstanza);
      Navigator.pop(context);
    }
    else {
      showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: Text("Errore numero stanze massimo registrate"),
              content: Text(
                  " aumentare numero stanze del hotel"),
              actions: [
                TextButton(
                    onPressed: () {
                      Navigator.pop(context,widget.hotel);
                    },
                    child: Text('conferma')
                )
              ],
            );
          }
      );
    }
  }
}