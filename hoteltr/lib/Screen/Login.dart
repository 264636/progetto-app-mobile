import 'package:flutter/material.dart';
import 'package:hoteltr/Widget/base.dart';
import 'package:hoteltr/main.dart';
import 'package:provider/provider.dart';
import 'package:hoteltr/Screen/Pannelloctr.dart';
import '../DatabaseHelper/DataBaseHelper.dart';
import '../Models/models.dart';
import '../Sessioni.dart';

class Login extends StatefulWidget {
  Login({Key? key}) : super(key: key);

  @override
  _Login createState() => _Login();
}

class _Login extends State<Login>{
  TextEditingController Usernamectr = TextEditingController();
  TextEditingController Passwordctr = TextEditingController();

  @override
  Widget build(BuildContext context){
    final userSession = Provider.of<UserSession>(context,listen:true);
    return customscreen(messaggio: "Home",
        body: Card(
          child: Column(
            children:  <Widget>[
              Padding(
                  padding: EdgeInsets.only(top: 20),
                  child: Text("Welcome back",style: TextStyle(color: Colors.black),),
              ),
              TextFormField(
                controller: Usernamectr,
                maxLength: 10,
                decoration: InputDecoration(
                  labelStyle: TextStyle(color: Colors.black),
                  focusedBorder: OutlineInputBorder(
                      borderSide: BorderSide(color: Colors.black)),
                  labelText: "Username",
                  icon: Icon(
                    Icons.people,
                    color: Colors.blueGrey,
                  ),
                ),
              ),
              TextFormField(
                controller: Passwordctr,
                obscureText: true,
                maxLength: 10,
                decoration: InputDecoration(
                    labelStyle: TextStyle(color: Colors.black),
                    focusedBorder: OutlineInputBorder(
                        borderSide: BorderSide(color: Colors.black)),
                    labelText: "Password",
                    icon: Icon(
                      Icons.password,
                      color: Colors.blueGrey,
                    )),
              ),
              ElevatedButton(
                  onPressed: () {
                    verifica(Usernamectr.text, Passwordctr.text, userSession,);
                  },
                  child: Text("Login"))
            ],
          ),
        )
    );
  }
  void verifica(String username, String password, UserSession userSession) async {
    Utenti ut;
    String pw = generateMd5(password);
    DatabaseHelper dbhelper = DatabaseHelper();
    Set<Utenti> users = await dbhelper.getUtente();
    for (Utenti utente in users) {
      ut=utente;
      if (utente.Username == username && utente.Password == pw) {
        if (utente.Username == "admin") {
          setState(() {
            userSession.login(utente.Username);
          });
          Navigator.push(
              context, MaterialPageRoute(builder: (context) => Pannelloctr()));
          break;
        } else {
          setState(() {
            userSession.login(utente.Username);
          });
          Navigator.push(context,
              MaterialPageRoute(builder: (context) => MyhomePage(messagge: "My Hotel")));
          break;
        }

      }
      if(utente.Username != username && utente.Password != pw) {
        showDialog(
            context: context,
            builder: (BuildContext context) {
              return AlertDialog(
                title: Text("Errore nel Login"),
                content: Text("Utente non registrato o password errata"),
                actions: [
                  TextButton(
                      onPressed: () {
                        Navigator.pop(context);
                      },
                      child: Text('conferma'))
                ],
              );
            });
      }
    }
  }
}
