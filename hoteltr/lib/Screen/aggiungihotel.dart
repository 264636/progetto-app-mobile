import 'dart:typed_data';

import 'package:flutter/material.dart';
import 'package:hoteltr/Models/models.dart';
import 'package:hoteltr/Widget/base.dart';
import 'package:provider/provider.dart';
import '../DatabaseHelper/DataBaseHelper.dart';
import '../Sessioni.dart';
import 'package:image_picker/image_picker.dart';
import 'dart:io';

class aggiungihotel extends StatefulWidget {
  aggiungihotel({Key? key}) : super(key: key);

  @override
  _aggiungihotel createState() => _aggiungihotel();
}

class _aggiungihotel extends State<aggiungihotel> {
  DatabaseHelper dbhelper = DatabaseHelper();
  late Uint8List immagine;

  TextEditingController Nome = TextEditingController();
  TextEditingController Localita = TextEditingController();
  TextEditingController Apertura = TextEditingController();
  TextEditingController Chiusura = TextEditingController();
  TextEditingController Servizi = TextEditingController();
  TextEditingController Stanze = TextEditingController();
  TextEditingController Email = TextEditingController();

  Future<void> _selectImage() async {
    final picker = ImagePicker();
    final pickedFile = await picker.pickImage(source: ImageSource.gallery);
    if (pickedFile != null) {
      final bytes = await pickedFile.readAsBytes();
      setState(() {
        immagine = bytes;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    final userSession = Provider.of<UserSession>(context);
    final hotelSession = Provider.of<HotelSession>(context);
    // TODO: implement build
    return customscreen(
      body: Card(child:CustomScrollView(
        physics: AlwaysScrollableScrollPhysics(),
    slivers: <Widget>[
    SliverFillRemaining(
    child: ListView.builder(
    itemCount: 1,
    itemBuilder: (context, index) {
      return Column(
        children: <Widget>[
          TextFormField(
            controller: Nome,
            decoration: const InputDecoration(
              labelStyle: TextStyle(color: Colors.black),
              focusedBorder: OutlineInputBorder(
                borderSide: BorderSide(color: Colors.black),
              ),
              labelText: "Nome",
              icon: Icon(Icons.people, color: Colors.blueGrey),
            ),
          ),
          TextFormField(
            controller: Localita,
            decoration: const InputDecoration(
              labelStyle: TextStyle(color: Colors.black),
              focusedBorder: OutlineInputBorder(
                borderSide: BorderSide(color: Colors.black),
              ),
              labelText: "Localita",
              icon: Icon(Icons.people, color: Colors.blueGrey),
            ),
          ),
          TextFormField(
            controller: Apertura,
            decoration: const InputDecoration(
              labelStyle: TextStyle(color: Colors.black),
              focusedBorder: OutlineInputBorder(
                borderSide: BorderSide(color: Colors.black),
              ),
              labelText: "Apertura",
              icon: Icon(Icons.people, color: Colors.blueGrey),
            ),
          ),
          TextFormField(
            controller: Chiusura,
            decoration: const InputDecoration(
              labelStyle: TextStyle(color: Colors.black),
              focusedBorder: OutlineInputBorder(
                borderSide: BorderSide(color: Colors.black),
              ),
              labelText: "Chiusura",
              icon: Icon(Icons.people, color: Colors.blueGrey),
            ),
          ),
          TextFormField(
            controller: Servizi,
            decoration: const InputDecoration(
              labelStyle: TextStyle(color: Colors.black),
              focusedBorder: OutlineInputBorder(
                borderSide: BorderSide(color: Colors.black),
              ),
              labelText: "Servizi",
              icon: Icon(Icons.people, color: Colors.blueGrey),
            ),
          ),
          TextFormField(
            keyboardType: TextInputType.number,
            controller: Stanze,
            decoration: const InputDecoration(
              labelStyle: TextStyle(color: Colors.black),
              focusedBorder: OutlineInputBorder(
                borderSide: BorderSide(color: Colors.black),
              ),
              labelText: "Stanze",
              icon: Icon(Icons.people, color: Colors.blueGrey),
            ),
          ),
          TextFormField(
            controller: Email,
            decoration: const InputDecoration(
              labelStyle: TextStyle(color: Colors.black),
              focusedBorder: OutlineInputBorder(
                borderSide: BorderSide(color: Colors.black),
              ),
              labelText: "Email",
              icon: Icon(Icons.people, color: Colors.blueGrey),
            ),
          ),
          ElevatedButton(
            onPressed: () {
              _selectImage();
            },
            child: Text("Seleziona un'immagine"),
          ),
          ElevatedButton(
            onPressed: () {
              nuovohotel(
                  Nome.text,  //nome hotel
                  Localita.text, //poszione hotel
                  Apertura.text,  //data apertura
                  Chiusura.text,  //data chiusura
                  Servizi.text,   //servizi offerti
                  int.parse(Stanze.text), //nummero stanze
                  Email.text, //email hotel
                  userSession, // sessione usata per gestione utenti
                  hotelSession, // sessione usata per gestione hotel
                  immagine //immagine hotel
              );
            },
            child: const Text("Registra Il tuo Hotel"),
          ),
        ],
      );
    }
    )
    ),
    ],
    ),
    ),
      messaggio: 'Registra il tuo hotel',
    );
  }

  void nuovohotel(
      String Nome,
      String Localita,
      String Apertura,
      String Chiusura,
      String Servizi,
      int Stanze,
      String Email,
      UserSession adminSession,
      HotelSession hotelSession,
      Uint8List immagine) async {
    DateTime apertura=DateTime(DateTime.parse(Apertura).year,DateTime.parse(Apertura).month,DateTime.parse(Apertura).day);
    DateTime chiusura=DateTime(DateTime.parse(Chiusura).year,DateTime.parse(Chiusura).month,DateTime.parse(Chiusura).day);
    if (adminSession.isAdmin()) {
      Hotel hotel = Hotel(
          Nome: Nome,
          Localita: Localita,
          Apertura: apertura,
          Chiusura: chiusura,
          Servizi: Servizi,
          Descrizione: " ",
          Email: Email,
          Stanze: Stanze,
          Image: immagine);
      await dbhelper.InsertHotel(hotel);
      hotelSession.esistehotel(hotel);
      Navigator.pop(context);
    } else {
      showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: Text("Errore nella registrazione dell'hotel"),
              content: Text(
                  "Non sei admin non puoi creare nuovi hotel, registrati come admin"),
              actions: [
                TextButton(
                    onPressed: () {
                      Navigator.pop(context);
                    },
                    child: Text('conferma'))
              ],
            );
          });
    }
  }
}