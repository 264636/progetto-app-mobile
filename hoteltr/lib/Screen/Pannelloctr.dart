import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:hoteltr/Screen/aggiungihotel.dart';
import 'package:hoteltr/Screen/gestioneprenotazioni.dart';
import 'package:hoteltr/Widget/base.dart';
import 'package:provider/provider.dart';

import '../DatabaseHelper/DataBaseHelper.dart';
import '../Models/models.dart';
import '../Sessioni.dart';
import '../Widget/card.dart';
import 'aggiungistanza.dart';

class Pannelloctr extends StatefulWidget{
  Pannelloctr({Key? key}):super(key:key);

  @override
  _Pannelloctr createState()=>_Pannelloctr();

}

class _Pannelloctr extends State<Pannelloctr>{
  DatabaseHelper dbhelper = DatabaseHelper();
  List<Stanze> stanze=[];

  void updateStanze(Stanze newstanza){
    setState(() {
      loadstanze();
    });
  }
  void delStanza(Stanze stanza) async{
    await dbhelper.RimuoviStanza(stanza);
    setState(() {
      stanze.remove(stanza);
    });
    loadstanze();
  }

  void loadstanze() async{
      stanze=await dbhelper.getStanze();
      setState(() {});
  }
  @override
  void initState() {
    // TODO: implement initState
    loadstanze();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {

    final hotelSession = Provider.of<HotelSession>(context,listen:true);

    return customscreen(messaggio: 'Home',body:
    Container(
      color: Colors.orange[100],
      child: CustomScrollView(
        physics: AlwaysScrollableScrollPhysics(),
        slivers: <Widget>[
          SliverToBoxAdapter(
            child: Container(
              alignment: Alignment.center,
              height: 50.0,
              child: FutureBuilder<bool>(
                  future: hotelSession.eshotel(),
                  builder: (context,snapshot){
                    if(snapshot.hasData && snapshot.data!){
                      return  Row(
                        children: <Widget>[
                          Expanded(child: ElevatedButton(onPressed:(){Navigator.push(context, MaterialPageRoute(builder: (context)=>aggiungistanza(hotel: hotelSession.hotel, newstanza: updateStanze, stanze: stanze)));} , child: Text("Aggiungi Stanza"),)),
                          Expanded(child: ElevatedButton(onPressed: (){Navigator.push(context, MaterialPageRoute(builder: (context)=>gestioneprenotazioni()));}, child: Text("Visualizza Prenotazioni"),)),
                          Expanded(child: IconButton(onPressed: loadstanze,icon:Icon(Icons.repeat),))
                        ],
                      );
                    }
                    return Row(children: <Widget>[
                      Text("Nessun hotel registrato aggiungine uno",),
                      ElevatedButton(onPressed: (){Navigator.push(context, MaterialPageRoute(builder: (context)=>aggiungihotel()));}, child: Text("Aggiungi Hotel",style: TextStyle(color: Colors.white)))
                    ],);
                  }
              ),
            ),
          ),
          SliverFillRemaining(
            child: ListView.builder(
              itemCount: stanze.length,
              itemBuilder: (BuildContext context,int index){
                return ListTile(
                    title: CustomCard(stanza: stanze[index],messaggio: "Modifica Stanza",)
                );
              },
            ),
          ),
        ],
      ),) ,);
  }

}