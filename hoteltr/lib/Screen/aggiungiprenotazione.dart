import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:hoteltr/DatabaseHelper/DataBaseHelper.dart';
import 'package:hoteltr/Models/models.dart';
import 'package:hoteltr/Widget/base.dart';
import 'package:hoteltr/Widget/card.dart';
import 'package:provider/provider.dart';

import '../Sessioni.dart';
import '../main.dart';
import 'Pannelloctr.dart';


class aggiungiprenotazione extends StatefulWidget{
  final Stanze stanza;
  aggiungiprenotazione({Key? key,required this.stanza});

  @override
  _aggiungiprenotazione createState()=> _aggiungiprenotazione();
}

class _aggiungiprenotazione extends State<aggiungiprenotazione>{
  int _currentIndex = 0;

  DateTime? _selectedDate;
  DateTime? _selectedDatePartenza;
  late List<Prenotazioni> pren;

  DatabaseHelper dbhelper=new DatabaseHelper();



  Widget build(BuildContext context){
    final userSession= Provider.of<UserSession>(context, listen:true);
    final hotelSession= Provider.of<HotelSession>(context, listen:true);


    Future<void> _selectDate(BuildContext context) async{

      DateTime now = DateTime.now();
      DateTime initialDate = DateTime(now.year, now.month, now.day + 1);
      List<DateTime> _disabledDates= await dbhelper.getDateOccupate(widget.stanza.IdStanza);
      DateTime FirstselectableDate(DateTime Date)
      {
        for(DateTime d in _disabledDates){
          if(DateTime(d.year,d.month,d.day) == DateTime(Date.year,Date.month,Date.day)){
            Date=DateTime(Date.year,Date.month,Date.day+1);
          }
        }
        return Date;
      }
      final DateTime? pickedDate = await showDatePicker(
          context: context,
          initialDate: FirstselectableDate(initialDate), //devo creare una funzione che torna la data possibile per inziare a selezionare
          firstDate:FirstselectableDate(DateTime.now()),
          lastDate: hotelSession.hotel.Chiusura!,
        selectableDayPredicate: (DateTime date){
            if(_disabledDates.contains(date)){
              return false;
            }
            return true;
        }
      );
      if(pickedDate!=null){
        setState(() {
          _selectedDate=pickedDate;
        });
      }
    }
    Future<void> _selectDatepartenza(BuildContext context) async{
      List<DateTime> _disabledDates=await dbhelper.getDateOccupate(widget.stanza.IdStanza);
      final DateTime? pickedDate = await showDatePicker(
          context: context,
          initialDate: DateTime(_selectedDate!.year,_selectedDate!.month,_selectedDate!.day+1),
          firstDate: _selectedDate!,
          lastDate: hotelSession.hotel.Chiusura!,
          selectableDayPredicate: (DateTime date){
            if(_disabledDates.contains(date)){
              return false;
            }
            return true;
          }
      );
      if(pickedDate!=null){
        setState(() {
          _selectedDatePartenza=pickedDate;
        });
      }
    }

    return Scaffold(body:Card(
       child: ListTile(
         title:Container(
             decoration: BoxDecoration(
               border: Border.all(
                 color: Colors.black.withOpacity(0.5),
                 width: 2.0,
               ),
               borderRadius: BorderRadius.circular(10.0),
             ),
             child:
             ClipRRect(
                 borderRadius: BorderRadius.circular(10),
                 child:Image.memory(widget.stanza.Image,fit: BoxFit.fitHeight,)
             )
         ),
         subtitle: Padding(
           padding: EdgeInsets.only(left: 20),
           child: Column(
             mainAxisAlignment: MainAxisAlignment.start,
             crossAxisAlignment: CrossAxisAlignment.start,
             children: <Widget>[
               Text(
                   "Servizi: "+widget.stanza.Servizi,
                   style:TextStyle(color: Colors.blueGrey)
               ),

               Text(
                   "Numero posti: "+widget.stanza.Nposti.toString(),
                   style:TextStyle(color: Colors.blueGrey)
               ),

               Text(
                   "Prezzo per notte: "+widget.stanza.Prezzo.toString()+'€',
                   style:TextStyle(color: Colors.blueGrey)
               ),
               Row(children: <Widget>[
                 Padding(padding:EdgeInsets.only(left: 10), child:ElevatedButton(onPressed: (){_selectDate(context);}, child: Text("Data Arrivo"))),
                 Padding(padding:EdgeInsets.only(left: 10), child:ElevatedButton(onPressed: (){_selectDatepartenza(context);}, child: Text("Data Partenza"))),
               ],),
               Padding(padding:EdgeInsets.only(left:10), child:ElevatedButton(onPressed: (){nuovaprenotazione(widget.stanza, _selectedDate!, _selectedDatePartenza!, userSession, hotelSession);}, child: Text("Prenota"))),
               // ElevatedButton(onPressed: (){Navigator.push(context, MaterialPageRoute(builder: (context)=> aggiungiprenotazione(stanze: stanze[index],utente: widget.utente,)));}, child: Text("Prenota"))
             ],
           ),
         ),
       )
   ),
     bottomNavigationBar: BottomNavigationBar(
       currentIndex: _currentIndex,
       onTap: (int index) {
         setState(() {
           _currentIndex = index;
         });

         // Azioni specifiche per ogni elemento del BottomNavigationBar
         if (index == 0) {
           Navigator.push(context, MaterialPageRoute(builder: (context)=>MyhomePage(messagge: "My hotel")));
         } else if (index == 2) {
           if(userSession.isAdmin()){
             Navigator.push(context, MaterialPageRoute(builder: (context)=>Pannelloctr()));

           }
         }
         // Aggiungi altre condizioni per gli altri elementi del BottomNavigationBar se necessario
       },
       items: [
         BottomNavigationBarItem(
           icon: Icon(Icons.home),
           label: 'Home',

         ),
         BottomNavigationBarItem(
           icon: Icon(Icons.info),
           label: 'Informazioni',
         ),
         BottomNavigationBarItem(
           icon: Icon(Icons.person),
           label: 'Profilo',
         ),
       ],
     ),
   );
  }
  void nuovaprenotazione(Stanze stanza,DateTime datainizio,DateTime datafine,UserSession userSession,HotelSession hotelSession) async{
    var startTime = DateTime.now().millisecondsSinceEpoch;
    pren=await dbhelper.getPrenotazioni();
    int cont=pren.length;
    Prenotazioni prenotazione;
    if(userSession.isLogged){
      prenotazione= Prenotazioni(IdPrenotazione: cont, DataInizio: DateTime(datainizio.year,datainizio.month,datainizio.day), DataFine: DateTime(datafine.year,datafine.month,datafine.day), IdStanza: stanza.IdStanza, Username: userSession.username, Sconto: 20, Stato: 0);
    }else{
      prenotazione= Prenotazioni(IdPrenotazione: cont, DataInizio: DateTime(datainizio.year,datainizio.month,datainizio.day), DataFine: DateTime(datafine.year,datafine.month,datafine.day), IdStanza: stanza.IdStanza, Username: " ", Sconto: 0, Stato: 0);
    }
    await dbhelper.InsertPrenotazione(prenotazione);
    var endTime = DateTime.now().millisecondsSinceEpoch; // Fine del calcolo del tempo
    var executionTime = endTime - startTime;
    print('Tempo di esecuzione per inserimento di un nuovo record: $executionTime millisecondi');


    showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text("Conferma prenotazione"),
            content: Text("Ricordati di confermare la prenotazione all'email  "+ hotelSession.hotel.Email +"  entro 3 giorni "),
            actions: [
              TextButton(
                  onPressed: () {
                    Navigator.pop(context);
                    Navigator.pop(context);
                  },
                  child: Text('conferma'))
            ],
          );
        });
}
}