import 'package:flutter/cupertino.dart';
import 'package:hoteltr/DatabaseHelper/DataBaseHelper.dart';
import 'Models/models.dart';

class HotelSession extends ChangeNotifier{
  late Hotel hotel;

  bool _esisteHotel = false;

  bool get esisteHotel => _esisteHotel;

  Future<bool> eshotel() async{
    DatabaseHelper dbhelper = DatabaseHelper();
    hotel= await dbhelper.getHotel();
    if(hotel != null){
      _esisteHotel=true;
      return true;
    }
    return false;
  }

  void esistehotel(Hotel newhotel){
    _esisteHotel=true;
    hotel=newhotel;
    notifyListeners();
  }

  void nonesistehotel(){
    _esisteHotel = false;
    notifyListeners();
  }
}
class UserSession extends ChangeNotifier {
  bool _isLogged = false;
  String username=" ";
  bool _isAdmin = false;
  bool get isLogged => _isLogged;

  void login(String usercr) {
    _isLogged = true;
    username=usercr;
    if(usercr=="admin"){
      _isAdmin=true;
    }
    notifyListeners();
  }

  void logout() {
    _isLogged = false;
    username=" ";
    _isAdmin=false;
    notifyListeners();
  }
  bool isAdmin(){
    return _isAdmin;
  }
}
