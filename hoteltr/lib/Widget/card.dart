import 'package:flutter/material.dart';
import 'package:hoteltr/Models/models.dart';
import 'package:hoteltr/Sessioni.dart';
import '../DatabaseHelper/DataBaseHelper.dart';
import 'package:image_picker/image_picker.dart';
import 'package:flutter/cupertino.dart';
import 'package:provider/provider.dart';
import 'package:hoteltr/Screen/Pannelloctr.dart';

import '../Screen/aggiungiprenotazione.dart';

class CustomCard extends StatefulWidget{
  final Stanze stanza;
  final String messaggio;
  CustomCard({Key? key,required this.stanza,required this.messaggio}): super(key:key);

  @override
  _customcard createState()=> _customcard();
}
class _customcard extends State<CustomCard>{

  late Utenti? utente;

  @override
  Widget build(BuildContext context){
    final userSession = Provider.of<UserSession>(context,listen:true);
    DatabaseHelper dbhelper=new DatabaseHelper();

    void Azione(String message) {
      if(message == "Prenota" &&  userSession.isLogged){
        Navigator.push(context, MaterialPageRoute(builder: (context)=>aggiungiprenotazione(stanza:widget.stanza,)));
      }
      else if(userSession.isAdmin()){
        showDialog(
            context: context,
            builder: (BuildContext context){
          return AlertDialog(
            title: Text("Stai cancellando la  stanza " +
                widget.stanza.IdStanza.toString()),
            content: Text("Utente non registrato o password errata"),
            actions: [
              TextButton(
                  onPressed: () async {
                    await dbhelper.RimuoviStanza(widget.stanza);
                    Navigator.pop(context);
                  },
                  child: Text('conferma'))
            ],
          );
        }
        );
      }
    }
    return Card(
        child: ListTile(
          title:Container(
              decoration: BoxDecoration(
                border: Border.all(
                  color: Colors.black.withOpacity(0.5),
                  width: 2.0,
                ),
                borderRadius: BorderRadius.circular(10.0),
              ),
              child:
              ClipRRect(
                  borderRadius: BorderRadius.circular(10),
                  child:Image.memory(widget.stanza.Image,fit: BoxFit.fitHeight,)
              )
          ),
          subtitle: Padding(
            padding: EdgeInsets.only(left: 20),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text(
                    "Servizi: "+widget.stanza.Servizi,
                    style:TextStyle(color: Colors.blueGrey)
                ),

                Text(
                    "Numero posti: "+widget.stanza.Nposti.toString(),
                    style:TextStyle(color: Colors.blueGrey)
                ),

                Text(
                    "Prezzo per notte: "+widget.stanza.Prezzo.toString()+'€',
                    style:TextStyle(color: Colors.blueGrey)
                ),
                ElevatedButton(onPressed: (){Azione(widget.messaggio);}, child: Text(widget.messaggio))
              ],
            ),
          ),
        )
    );
    
  }

}

class CardPrenotazioni extends StatefulWidget{
  final Prenotazioni prenotazione;
  final Stanze stanza;
  final Function aggpr;
  final Function cambiastato;
  CardPrenotazioni({Key? key,required this.stanza,required this.aggpr,required this.prenotazione,required this.cambiastato}): super(key:key);

  @override
  _CardPrenotazioni createState()=> _CardPrenotazioni();
}

class _CardPrenotazioni extends State<CardPrenotazioni>{
  DatabaseHelper dbhelper =new DatabaseHelper();
  void deletepr(Prenotazioni pr){
    dbhelper.RimuoviPrenotazione(pr);
    widget.aggpr();
  }
  int Prezzotot(DateTime inizio,DateTime fine,int prezzo){

    Duration difference = fine.difference(inizio); // Calcoliamo la differenza tra le due date

    int days = difference.inDays;

    return days*prezzo;
  }

  @override
  Widget build(BuildContext context) {
    final userSession = Provider.of<UserSession>(context, listen: true);
    Future<bool> isadmin() async {
      if (userSession.isAdmin()) {
        return true;
      }
      else
        return false;
    }

    Color iconColor(Prenotazioni pr){
      if(pr.Stato==0){
        return Colors.orange;
      }
      return Colors.green;
    }

    return FutureBuilder<bool>(
      future: isadmin(),
      builder: (context, snapshot) {
        if (snapshot.hasData && snapshot.data!) {
          return Card(
              child: ListTile(
                title: Container(
                    decoration: BoxDecoration(
                      border: Border.all(
                        color: Colors.black.withOpacity(0.5),
                        width: 2.0,
                      ),
                      borderRadius: BorderRadius.circular(10.0),
                    ),
                    child:
                    ClipRRect(
                        borderRadius: BorderRadius.circular(10),
                        child: Image.memory(
                          widget.stanza.Image, fit: BoxFit.fitHeight,)
                    )
                ),
                subtitle: Padding(
                  padding: EdgeInsets.only(left: 20),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text(
                          "Servizi: " + widget.stanza.Servizi,
                          style: TextStyle(color: Colors.blueGrey)
                      ),

                      Text(
                          "Numero posti: " + widget.stanza.Nposti.toString(),
                          style: TextStyle(color: Colors.blueGrey)
                      ),

                      Text(
                          "Prezzo totale: " + Prezzotot(
                              widget.prenotazione.DataInizio,
                              widget.prenotazione.DataFine,
                              widget.stanza.Prezzo).toString() + '€',
                          style: TextStyle(color: Colors.blueGrey)
                      ),
                      Text(
                          "Utente: " + widget.prenotazione.Username,
                          style: TextStyle(color: Colors.blueGrey)
                      ),
                      Text(
                          "inizio soggiorno: " + widget.prenotazione.DataInizio
                              .year.toString() + "-" +
                              widget.prenotazione.DataInizio.month.toString() +
                              "-" +
                              widget.prenotazione.DataInizio.day.toString(),
                          style: TextStyle(color: Colors.blueGrey)
                      ),
                      Text(
                          "fine soggiorno: " + widget.prenotazione.DataFine.year
                              .toString() + "-" +
                              widget.prenotazione.DataFine.month.toString() +
                              "-" + widget.prenotazione.DataFine.day.toString(),
                          style: TextStyle(color: Colors.blueGrey)
                      ),
                      ElevatedButton(onPressed: () {
                        showDialog(
                            context: context,
                            builder: (BuildContext context) {
                              return AlertDialog(
                                title: Text("Modifica prenotazione"),
                                content: Text("Accetta o cancella prenotazione"),
                                actions: [
                                  TextButton(
                                      onPressed: () {
                                        widget.cambiastato(widget.prenotazione,true);
                                        Navigator.pop(context);
                                      },
                                      child: Text('Accetta prenotazione')),

                                  TextButton(
                                      onPressed: () {
                                        widget.cambiastato(widget.prenotazione,false);
                                        Navigator.pop(context);
                                      },
                                      child: Text('Cancella prenotazione'))
                                ],
                              );
                            });
                      }, child: Text("Modifica prenotazione"))
                    ],
                  ),
                ),
              )
          );
        }
        return  Card( //card per gestione prenotazione utenti
            child: ListTile(
              title:Container(
                  decoration: BoxDecoration(
                    border: Border.all(
                      color: Colors.black.withOpacity(0.5),
                      width: 2.0,
                    ),
                    borderRadius: BorderRadius.circular(10.0),
                  ),
                  child:
                  ClipRRect(
                      borderRadius: BorderRadius.circular(10),
                      child:Image.memory(widget.stanza.Image,fit: BoxFit.fitHeight,)
                  )
              ),
              subtitle: Padding(
                padding: EdgeInsets.only(left: 20),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text(
                        "Servizi: "+widget.stanza.Servizi,
                        style:TextStyle(color: Colors.blueGrey)
                    ),

                    Text(
                        "Numero posti: "+widget.stanza.Nposti.toString(),
                        style:TextStyle(color: Colors.blueGrey)
                    ),

                    Text(
                        "Prezzo totale: "+Prezzotot(widget.prenotazione.DataInizio, widget.prenotazione.DataFine,widget.stanza.Prezzo ).toString()+'€',
                        style:TextStyle(color: Colors.blueGrey)
                    ),
                    Text(
                        "Utente: "+widget.prenotazione.Username,
                        style:TextStyle(color: Colors.blueGrey)
                    ),
                    Text(
                        "inizio soggiorno: "+widget.prenotazione.DataInizio.year.toString()+"-"+widget.prenotazione.DataInizio.month.toString()+"-"+widget.prenotazione.DataInizio.day.toString(),
                        style:TextStyle(color: Colors.blueGrey)
                    ),
                    Text(
                        "fine soggiorno: "+widget.prenotazione.DataFine.year.toString()+"-"+widget.prenotazione.DataFine.month.toString()+"-"+widget.prenotazione.DataFine.day.toString(),
                        style:TextStyle(color: Colors.blueGrey)
                    ),
                    //va ridefinita la funzione deletepr cioè almeno deve essere inserita in uno schema cioè prima deve chiedere se cambiare stato o eliminare la prenotazione prima di eliminarle e ricorda cche se la elimina non deve esistere più da nessuna parte.
                    Row(children: <Widget>[
                      Container(
                    color: Colors.white, // Imposta il colore di sfondo del container
                      child: Icon(
                        Icons.check_circle,
                        color: iconColor(widget.prenotazione), // Imposta il colore dell'icona
                        size: 32.0,
                      ),
                    ),
                      ElevatedButton(onPressed: (){deletepr(widget.prenotazione);}, child: Text("Cancella prenotazione")),
                    ],
              )
                  ],
                ),
              ),
            )
        );
      },
    );
  }
}
