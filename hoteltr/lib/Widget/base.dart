import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:hoteltr/Models/models.dart';
import 'package:hoteltr/Screen/Pannelloctr.dart';
import 'package:hoteltr/Screen/Registrazione.dart';
import 'package:hoteltr/Screen/gestioneprenotazioni.dart';
import 'package:hoteltr/main.dart';
import 'package:provider/provider.dart';

import '../DatabaseHelper/DataBaseHelper.dart';
import '../Screen/Login.dart';
import '../Sessioni.dart';

class customscreen extends StatefulWidget {
  final String messaggio;
  final Widget body;

  customscreen({Key? key, required this.messaggio, required this.body})
      : super(key: key);

  @override
  _customscreen createState() => _customscreen();
}

class _customscreen extends State<customscreen> {
  int _currentIndex = 0;
  List<Stanze> stanze = [];
  DatabaseHelper dbhelper = DatabaseHelper();

  Future<bool> Userislogged(UserSession userSession) async {
    return userSession.isLogged;
  }

  void logout(UserSession userSession) async {
    setState(() {
      userSession.logout();
      Navigator.push(
          context,
          MaterialPageRoute(
              builder: (context) => MyhomePage(messagge: "My hotel")));
    });
  }

  @override
  Widget build(BuildContext context) {
    final hotelSession = Provider.of<HotelSession>(context, listen: true);
    final userSession = Provider.of<UserSession>(context, listen: true);

    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: false,
        toolbarHeight: 100,
        title: FutureBuilder<bool>(
            future: Userislogged(userSession),
            builder: (context, snapshot) {
              if (snapshot.hasData && snapshot.data!) {
                return SizedBox(
                  width: 350,
                  height: 100,
                  child: Stack(
                    children: <Widget>[
                      Positioned(
                        top: 50,
                        child: Text(
                          widget.messaggio,
                          style: const TextStyle(
                              color: Colors.white,
                              fontSize: 24,
                              fontWeight: FontWeight.bold,
                              fontStyle: FontStyle.italic,
                              letterSpacing: 1.5,
                              wordSpacing: 2),
                        ),
                      ),
                      Positioned(
                          top: 10,
                          left: 250,
                          child: ElevatedButton(
                            onPressed: () {
                              logout(userSession);
                            },
                            child: Text(
                              "Logout",
                              style: TextStyle(color: Colors.white),
                            ),
                          ))
                    ],
                  ),
                );
              }
              return SizedBox(
                width: 350,
                height: 100,
                child: Stack(
                  children: <Widget>[
                    Positioned(
                      top: 50,
                      child: Text(
                        widget.messaggio,
                        style: const TextStyle(
                            color: Colors.white,
                            fontSize: 24,
                            fontWeight: FontWeight.bold,
                            fontStyle: FontStyle.italic,
                            letterSpacing: 1.5,
                            wordSpacing: 2),
                      ),
                    ),
                    Positioned(
                        top: -20,
                        left: -30,
                        child: Container(
                          width: 60,
                          height: 60,
                          decoration: const BoxDecoration(
                              shape: BoxShape.circle,
                              color: Color.fromRGBO(255, 255, 255, 0.5)),
                        )),
                    Positioned(
                        top: -30,
                        left: -10,
                        child: Container(
                          width: 60,
                          height: 60,
                          decoration: const BoxDecoration(
                              shape: BoxShape.circle,
                              color: Color.fromRGBO(255, 255, 255, 0.5)),
                        )),
                    Positioned(
                        top: 10,
                        left: 250,
                        child: ElevatedButton(
                          onPressed: () {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => Login()));
                          },
                          child: Text(
                            "Login",
                            style: TextStyle(color: Colors.white),
                          ),
                        )),
                    Positioned(
                        top: 50,
                        left: 250,
                        child: ElevatedButton(
                          onPressed: () {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => Registrazione()));
                          },
                          child: Text(
                            "Registrati",
                            style: TextStyle(color: Colors.white),
                          ),
                        ))
                  ],
                ),
              );
            }),
      ),
      body: widget.body,
      bottomNavigationBar: BottomNavigationBar(
        currentIndex: _currentIndex,
        onTap: (int index) {
          setState(() {
            _currentIndex = index;
          });

          // Azioni specifiche per ogni elemento del BottomNavigationBar
          if (index == 0) {
            Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) => MyhomePage(messagge: "My hotel")));
          } else if (index == 2) {
            if (userSession.isAdmin()) {
              Navigator.push(context,
                  MaterialPageRoute(builder: (context) => Pannelloctr()));
            } else {
              Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => gestioneprenotazioni()));
            }
          }
        },
        items: [
          BottomNavigationBarItem(
            icon: Icon(Icons.home),
            label: 'Home',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.info),
            label: 'Informazioni',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.person),
            label: 'Profilo',
          ),
        ],
      ),
    );
  }
}
