import 'dart:io';

import 'package:hoteltr/Models/models.dart';
import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';
import 'package:path/path.dart';
import 'dart:async';

class DatabaseHelper {
  static final DatabaseHelper _instance = DatabaseHelper._internal();
  factory DatabaseHelper() => _instance;

  static Database? _db;

  Future<Database> get database async {
    if (_db != null) {
      return _db!;
    }
    _db = await _initDatabase();
    return _db!;
  }

  DatabaseHelper._internal();

  Future<Database> _initDatabase() async {
    final darabasePath = await getDatabasesPath();
    final path = join(darabasePath, 'My_hotel.db');
    return await openDatabase(path, version: 1, onCreate: (db, version) async {
      await Hotel.createTable(db, version);
      await Stanze.createTable(db, version);
      await Prenotazioni.createTable(db, version);
      await Utenti.createTable(db, version);

      final result =
          await db.rawQuery('SELECT * FROM Utenti WHERE username="admin"');
      if (result.isEmpty) {
        await db.insert('Utenti', {
          'Username': 'admin',
          'Password': generateMd5('admin'),
          'Sconto': 100
        });
      }
    });
  }

  //inserisco i metodi per inserire,estrarre,rimuovere gli hotel dal db
  Future<void> InsertHotel(Hotel hotel) async {
    final db = await database;
    await db.insert(
      'Hotel',
      hotel.toMap(),
      conflictAlgorithm: ConflictAlgorithm.replace,
    );
  }

  Future<Hotel> getHotel() async {
    final db = await database;
    final List<Map<String, dynamic>> maps = await db.query('Hotel');
    return Hotel.fromMap(maps[0]);
  }

  Future<void> RimuoviHotel() async {
    final db = await database;
    await db.execute('DELETE FROM Hotel');
  }

  // stessi metodi per le Stanze
  Future<void> InsertStanza(Stanze stanze) async {
    final db = await database;
    await db.insert(
      'Stanze',
      stanze.toMap(),
      conflictAlgorithm: ConflictAlgorithm.replace,
    );
  }

  Future<List<Stanze>> getStanze() async {
    final db = await database;
    final List<Map<String, dynamic>> maps = await db.query('Stanze');
    return List.from(maps.map((map) => Stanze.fromMap(map)));
  }

  Future<void> RimuoviStanza(Stanze stanze) async {
    final db = await database;
    await db
        .delete('Stanze', where: 'IdStanza = ?', whereArgs: [stanze.IdStanza]);
    await db.delete('Prenotazioni',
        where: 'IdStanza = ?', whereArgs: [stanze.IdStanza]);
  }

  // stessi metodi per le Prenotazioni
  Future<void> InsertPrenotazione(Prenotazioni prenotazioni) async {
    final db = await database;
    await db.insert(
      'Prenotazioni',
      prenotazioni.toMap(),
      conflictAlgorithm: ConflictAlgorithm.replace,
    );
  }

  Future<List<Prenotazioni>> getPrenotazioni() async {
    final db = await database;
    final List<Map<String, dynamic>> maps = await db.query('Prenotazioni');
    return List.from(maps.map((map) => Prenotazioni.fromMap(map)));
  }

  Future<void> RimuoviPrenotazione(Prenotazioni prenotazioni) async {
    final db = await database;
    await db.delete('Prenotazioni',
        where: 'IdPrenotazione = ?', whereArgs: [prenotazioni.IdPrenotazione]);
  }

  Future<void> CambiaPrenotazione(Prenotazioni prenotazioni) async {
    final db = await database;
    db.update('Prenotazioni', prenotazioni.toMap(),
        where: 'IdPrenotazione = ?', whereArgs: [prenotazioni.IdPrenotazione]);
  }

  // stessi metodi per gli Utenti
  Future<void> InsertUtente(Utenti utente) async {
    final db = await database;
    await db.insert(
      'Utenti',
      utente.toMap(),
      conflictAlgorithm: ConflictAlgorithm.replace,
    );
  }

  Future<Set<Utenti>> getUtente() async {
    final db = await database;
    final List<Map<String, dynamic>> maps = await db.query('Utenti');
    return Set.from(maps.map((map) => Utenti.fromMap(map)));
  }

  Future<void> RimuoviUtente(Utenti utente) async {
    final db = await database;
    await db
        .delete('Utenti', where: 'Username = ?', whereArgs: [utente.Username]);
  }

  Future<List<Prenotazioni>> getPrenotazioniUtente(String Username) async {
    List<Prenotazioni> prenotazioni = [];

    final db = await database;

    List<Map<String, dynamic>> results = await db
        .rawQuery('SELECT * FROM Prenotazioni WHERE Username = ?', [Username]);

    for (Map<String, dynamic> row in results) {
      Prenotazioni pr = Prenotazioni.fromMap(row);
      prenotazioni.add(pr);
    }
    return prenotazioni;
  }

  Future<List<DateTime>> getDateOccupate(int IdStanza) async {
    List<DateTime> dates = [];

    final db = await database;

    List<Map<String, dynamic>> results =
        await db.query('Prenotazioni', columns: ['DataInizio']);
    List<Map<String, dynamic>> results1 =
        await db.query('Prenotazioni', columns: ['DataFine']);
    List<Map<String, dynamic>> idstanza =
        await db.query('Prenotazioni', columns: ['IdStanza']);

    for (Map<String, dynamic> row in results) {
      for (Map<String, dynamic> row1 in results1) {
        for (Map<String, dynamic> Idstanza in idstanza) {
          if (Idstanza['IdStanza'] == IdStanza) {
            DateTime startDate = DateTime(
                DateTime.parse(row['DataInizio']).year,
                DateTime.parse(row['DataInizio']).month,
                DateTime.parse(row['DataInizio']).day);
            DateTime endDate = DateTime(
                DateTime.parse(row1['DataFine']).year,
                DateTime.parse(row1['DataFine']).month,
                DateTime.parse(row1['DataFine']).day);

            // Add all dates between start and end date
            while (startDate.isBefore(endDate)) {
              dates.add(startDate);
              startDate = startDate.add(Duration(days: 1));
            }

            // Add end date
            dates.add(endDate);
          }
        }
      }
    }

    return dates;
  }
}
