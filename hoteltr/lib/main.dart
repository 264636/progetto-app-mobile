import 'package:flutter/material.dart';
import 'package:hoteltr/Widget/base.dart';
import 'package:hoteltr/Widget/card.dart';
import 'package:hoteltr/DatabaseHelper/DataBaseHelper.dart';
import 'package:hoteltr/Models/models.dart';
import 'package:hoteltr/Sessioni.dart';
import 'package:provider/provider.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  //qui devi inserire un initState per inizializzare la sessione dell'hotel perchè senno ogni volta abbiamo 800 hotel diversi
  final HotelSession hotelsession = HotelSession();
  final UserSession userssession = UserSession();
  Color myColor = Color(0xFF00FFFF);

  MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider.value(value: hotelsession),
        ChangeNotifierProvider.value(value: userssession),
      ],
      child: MaterialApp(
        title: "Flutter demo",
        theme: ThemeData(primarySwatch: Colors.cyan),
        home: MyhomePage(
          messagge: "MyHotel",
          hotelSession: hotelsession,
          userSession: userssession,
        ),
      ),
    );
  }
}

class MyhomePage extends StatefulWidget {
  late HotelSession? hotelSession;
  late UserSession? userSession;
  final String messagge;

  MyhomePage(
      {Key? key, required this.messagge, this.hotelSession, this.userSession})
      : super(key: key);

  @override
  _MyhomePage createState() => _MyhomePage();
}

class _MyhomePage extends State<MyhomePage> {

  late int startTime;
  late int endTime;
  late int executionTime;

  List<Stanze> stanze = [];

  DatabaseHelper dbhelper = DatabaseHelper();

  void loadhotel() async {
    widget.hotelSession?.eshotel();
    setState(() {});
  }

  void loadstanze() async {
    DateTime startTime = DateTime.now();

    stanze = await dbhelper.getStanze();
    setState(() {});

    DateTime endTime = DateTime.now(); // Fine del calcolo del tempo
    double executionTime = endTime.difference(startTime).inMilliseconds / 1000.0 ; // Calcola il tempo trascorso in secondi come numero decimale
    print('Tempo di esecuzione per estrazione dati: $executionTime millisecondi');
  }

  @override
  void initState() {
    super.initState();
    loadhotel();
    loadstanze();
  }

  @override
  Widget build(BuildContext context) {
    Stopwatch stopwatch = Stopwatch()..start();

    return customscreen(
      messaggio: widget.messagge,
      body: Container(
        color: Colors.orange[100],
        child: CustomScrollView(
          physics: AlwaysScrollableScrollPhysics(),
          slivers: <Widget>[
            SliverFillRemaining(
              child: ListView.builder(
                itemCount: stanze.length,
                itemBuilder: (BuildContext context, int index) {
                  return ListTile(
                      title: CustomCard(
                          stanza: stanze[index], messaggio: "Prenota"));
                },
              ),
            ),
          ],
        ),
      ),
    );
  }
}
