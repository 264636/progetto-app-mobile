import { SHA256 } from 'crypto-js';
import { encode } from 'utf8';

 export class Hotel{
  constructor({
    Nome,
    Localita,
    Apertura,
    Chiusura,
    Servizi,
    Descrizione,
    Email,
    Stanze
  })

  {
    this.Nome=Nome;
    this.Localita=Localita;
    this.Apertura=Apertura;
    this.Chiusura=Chiusura;
    this.Servizi=Servizi;
    this.Descrizione=Descrizione;
    this.Email=Email;
    this.Stanze=Stanze;
 }

 static createTable(db){
   return new Promise((resolve,reject) => {
      db.transaction((tx) => {
       tx.executeSql(
       `
        CREATE TABLE IF NOT EXISTS Hotel(
          Nome TEXT PRIMARY KEY,
          Localita TEXT,
          Apertura TEXT,
          Chiusura TEXT,
          Servizi TEXT,
          Descrizione TEXT,
          Email TEXT,
          Stanze INTEGER
        )
       `,
       [],
       ()=>{
       console.log("tabella Hotel creata con successo");
       resolve();
       },
       (_,error) => {
         console.log("Errore nella creazione della tabella Hotel",error);
         reject(error);
       }
       );
      tx.executeSql(`
                            INSERT INTO Hotel ( Nome,Localita,Apertura,Chiusura,Servizi,Descrizione,Email,Stanze)
                            VALUES (?, ?, ?, ?, ?, ?, ?, ?);
                            `, ['Sole', 'Mare', 'Feb 19, 2024', 'Dec 21, 2024', 'Tutti','Hotel sul mare vicino al centro','sole@gmail.com', 10]
      );
      });
   });
 }
  toMap() {
      return {
        Nome: this.Nome,
        Localita: this.Localita,
        Apertura: this.Apertura.toISOString(),
        Chiusura: this.Chiusura.toISOString(),
        Servizi: this.Servizi,
        Descrizione: this.Descrizione,
        Email: this.Email,
        Stanze: this.Stanze,
      };
    }

    static fromMap(map) {
      return new Hotel({
        Nome: map.Nome,
        Localita: map.Localita,
        Apertura: new Date(map.Apertura),
        Chiusura: new Date(map.Chiusura),
        Servizi: map.Servizi,
        Descrizione: map.Descrizione,
        Email: map.Email,
        Stanze: map.Stanze,
      });
    }
}

export class Stanze{
   constructor({
      IdStanza,
      Servizi,
      Nposti,
      Prezzo,
      Image,
   }) {
      this.IdStanza=IdStanza;
      this.Servizi=Servizi;
      this.Nposti=Nposti;
      this.Prezzo=Prezzo;
      this.Image=Image;
   }

   static createTable(db) {
                              return new Promise((resolve, reject) => {
                                db.transaction((tx) => {
                                  tx.executeSql(
                                    `
                                    CREATE TABLE IF NOT EXISTS Stanze (
                                                 IdStanza INTEGER PRIMARY KEY,
                                                 Servizi TEXT,
                                                 Nposti INTEGER,
                                                 Prezzo INTEGER,
                                                 Image BLOB
                                               )
                                    `,
                                    [],
                                    () => {
                                      console.log('Tabella Stanze creata con successo');
                                      resolve();
                                    },
                                    (_, error) => {
                                      console.log('Errore durante la creazione della tabella Stanze:', error);
                                      reject(error);
                                    }
                                  );
                                });
                              });
     }
     toMap() {
         return {
           IdStanza: this.IdStanza,
           Servizi: this.Servizi,
           Nposti: this.Nposti,
           Prezzo: this.Prezzo,
           Image: this.Image
         };
       }

       static fromMap(map) {
         return new Stanze({
           IdStanza: map.IdStanza,
           Servizi: map.Servizi,
           Nposti: map.Nposti,
           Prezzo: map.Prezzo,
           Image: map.Image,
         });
       }

}


export class Prenotazioni{
    constructor({
       IdPrenotazione,
       DataInizio,
       DataFine,
       IdStanza,
       Username,
       Stato,
    }) {
       this.IdPrenotazione=IdPrenotazione;
       this.DataInizio=DataInizio;
       this.DataFine=DataFine;
       this.IdStanza=IdStanza;
       this.Username=Username;
       this.Stato=Stato;
    }

    static createTable(db) {
        db.transaction((tx) => {
          tx.executeSql(
            `
            CREATE TABLE IF NOT EXISTS Prenotazioni (
              IdPrenotazione INTEGER PRIMARY KEY,
              DataInizio TEXT,
              DataFine TEXT,
              IdStanza INTEGER,
              Username TEXT,
              Stato INTEGER,
              FOREIGN KEY (IdStanza) REFERENCES Stanze(IdStanza),
              FOREIGN KEY (Username) REFERENCES Utenti(Username)
            )
            `,
            [],
            () => {
              console.log('Tabella Prenotazioni creata con successo');
            },
            (_, error) => {
              console.log('Errore durante la creazione della tabella Prenotazioni:', error);
            }
          );
        });
      }
      toMap() {
          return {
            IdPrenotazione: this.IdPrenotazione,
            DataInizio : this.DataInizio.toISOString(),
            DataFine: this.DataFine.toISOString(),
            IdStanza: this.IdStanza,
            Username: this.Username,
            Stato: this.Stato,
          };
        }

        static fromMap(map) {
          return new Prenotazioni({
            IdPrenotazione: map.IdPrenotazione,
            DataInizio: new Date(map.DataInizio),
            DataFine: new Date(map.DataFine),
            IdStanza: map.IdStanza,
            Username: map.Username,
            Stato: map.Stato,
          });
        }

 }

export class Utenti {

      hashPassword(password) {
      const hashedPassword = SHA256(password).toString();
      return hashedPassword;
      }

  constructor({ Username, Password, Sconto }) {
    this.Username = Username;
    this.Password = Password;
    this.Sconto = Sconto;
  }

  static createTable(db) {
    db.transaction((tx) => {
      tx.executeSql(
        `
        CREATE TABLE IF NOT EXISTS Utenti (
          Username TEXT PRIMARY KEY,
          Password TEXT,
          Sconto INTEGER
        );
        `,
        [],
         () => {
                       console.log('Tabella Utenti creata con successo');
                     },
                     (_, error) => {
                       console.log('Errore durante la creazione della tabella Prenotazioni:', error);
                     }
      );

      const hashedPassword = hashPassword('admin');

      tx.executeSql(
        `
        INSERT INTO Utenti (Username, Password, Sconto)
        VALUES (?, ?, ?);
        `,
        ['admin', hashedPassword, 100]
      );
    });
  }
}

export function hashPassword(password) {
    const hashedPassword = SHA256(password).toString();
    return hashedPassword;
    }
