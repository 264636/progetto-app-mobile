import {Hotel,Stanze,Prenotazioni,Utenti,Model} from './Models.js';
import React, { useEffect } from 'react';
import * as SQLite from 'expo-sqlite';

export class Databasehelper{

  async inizializzaDatabase(db){
   await Hotel.createTable(db);
   await Stanze.createTable(db);
   await Prenotazioni.createTable(db);
   await Utenti.createTable(db);
  }




 async  Insertrecord(tableName, data, db) {
   const columns = Object.keys(data).join(', ');
   const placeholders = Object.keys(data).map(() => '?').join(', ');
   const values = Object.values(data);

   try {
     await new Promise((resolve, reject) => {
       db.transaction((tx) => {
         tx.executeSql(
           `INSERT INTO ${tableName} (${columns}) VALUES (${placeholders})`,
           values,
           (_, result) => {
             resolve(result);
           },
           (_, error) => {
             reject(error);
           }
         );
       }, (error) => {
         console.log('Errore durante la transazione:', error);
       }, () => {
         console.log('Transazione completata con successo');
       });
     });
     console.log('Record aggiunto con successo');
   } catch (error) {
     console.log('Errore durante l\'inserimento:', error);
   }
 }
//funzioni che gestiscono utenti
  async InserUtente(utente,db){
    await this.Insertrecord('Utenti',utente,db);
  }

    async getAllUtenti(db) {
        return new Promise((resolve, reject) => {
          db.transaction((tx) => {
            tx.executeSql(
              `
              SELECT * FROM Utenti;
              `,
              [],
              (tx, result) => {
                const utenti = [];
                for (let i = 0; i < result.rows.length; i++) {
                  const row = result.rows.item(i);
                  utenti.push({
                    Username: row.Username,
                    Password: row.Password,
                    Sconto: row.Sconto,
                  });
                }
                resolve(utenti);
              },
              (_, error) => {
                reject(error);
              }
            );
          });
        });
      }

      //funzionni che gestiscono hotel
       async InserHotel(hotel,db){
          await this.Insertrecord('Hotel',hotel,db);
        }

          async getAllHotel(db) {
              return new Promise((resolve, reject) => {
                db.transaction((tx) => {
                  tx.executeSql(
                    `
                    SELECT * FROM Hotel;
                    `,
                    [],
                    (tx, result) => {
                      const hotel = [];
                      for (let i = 0; i < result.rows.length; i++) {
                        const row = result.rows.item(i);
                        hotel.push({
                                    Nome: row.Nome,
                                                            Localita: row.Localita,
                                                            Apertura: row.Apertura,
                                                            Chiusura: row.Chiusura,
                                                            Servizi: row.Servizi,
                                                            Descrizione: row.Descrizione,
                                                            Email: row.Email,
                                                            Stanze: row.Stanze,
                        });
                      }
                      resolve(hotel);
                    },
                    (_, error) => {
                      reject(error);
                    }
                  );
                });
              });
            }

            //funzioni che gestiscono Stanze
             async InserStanza(stanza,db){
             console.log("qui ci arrivo");
                      await this.Insertrecord('Stanze',stanza,db);
                    }

                      async getAllStanza(db) {
                          return new Promise((resolve, reject) => {
                            db.transaction((tx) => {
                              tx.executeSql(
                                `
                                SELECT * FROM Stanze;
                                `,
                                [],
                                (tx, result) => {
                                  const stanze = [];
                                  for (let i = 0; i < result.rows.length; i++) {
                                    const row = result.rows.item(i);
                                    stanze.push({
                                                           IdStanza: row.IdStanza,
                                                           Servizi: row.Servizi,
                                                           Nposti: row.Nposti,
                                                           Prezzo: row.Prezzo,
                                                           Image: row.Image,
                                    });
                                  }
                                  resolve(stanze);
                                },
                                (_, error) => {
                                  reject(error);
                                }
                              );
                            });
                          });
                        }
                        async deleteStanza(StanzaId){
                         try{
                            db.transaction((tx)=>{
                             tx.executeSql(
                                  `
                                   DELETE FROM Stanze WHERE IdStanza =?
                               `, [Stanzaid],
                              )
                               }); console.log('Cancellazione Stanza avvenuta') }catch(error){console.log('errore nella cancellazione della stanza');}
                        };

                        //funzioni che  gestiscono prenotazioni

                        async InserPrenotazioni(prenotazione,db){
                                              await this.Insertrecord('Prenotazioni',prenotazione,db);
                                            }

                                              async getAllPrenotazioni(db) {
                                                  return new Promise((resolve, reject) => {
                                                    db.transaction((tx) => {
                                                      tx.executeSql(
                                                        `
                                                        SELECT * FROM Prenotazioni;
                                                        `,
                                                        [],
                                                        (tx, result) => {
                                                          const prenotazioni = [];
                                                          for (let i = 0; i < result.rows.length; i++) {
                                                            const row = result.rows.item(i);
                                                            prenotazioni.push({
                                                                             IdPrenotazione: row.IdPrenotazione,
                                                                                         DataInizio : row.DataInizio.toISOString(),
                                                                                         DataFine: row.DataFine.toISOString(),
                                                                                         IdStanza: row.IdStanza,
                                                                                         Username: row.Username,
                                                                                         Stato: row.Stato,
                                                            });
                                                          }
                                                          resolve(prenotazioni);
                                                        },
                                                        (_, error) => {
                                                          reject(error);
                                                        }
                                                      );
                                                    });
                                                  });
                                                }
                                                async deletePrenotazioni(PrenotazioneId){
                                                   const db= await this.getDatabase();
                                                   await db.executeSql('DELETE FROM Prenotazioni WHERE IdPrenotazione =?', [PrenotazioneId])
                                                };

                                                async CambiaPrenotazione (prenotazioni) {
                                                  const db = await this.getDatabase();

                                                  try {
                                                    const values = prenotazioni.toMap();
                                                    const { IdPrenotazione, Stato } = values;

                                                    const columnsToUpdate = Object.keys(updatedValues);
                                                    const valuesToUpdate = Object.values(updatedValues);

                                                    const placeholders = columnsToUpdate.map(() => '?').join(', ');
                                                    const updateStatement = columnsToUpdate.map((column) => `${column} = ?`).join(', ');

                                                    const query = `UPDATE Prenotazioni SET ${updateStatement} WHERE IdPrenotazione = ?`;
                                                    const queryParams = [Stato, IdPrenotazione];

                                                    await db.executeSql(query, queryParams);

                                                    console.log('Prenotazione modificata correttamente');
                                                  } catch (error) {
                                                    console.error('Errore nella modifica della prenotazione', error);
                                                  }
                                                };


}