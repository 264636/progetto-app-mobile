import React, { createContext, useState } from 'react';
import { StyleSheet } from 'react-native';
import * as SQLite from 'expo-sqlite';


export const SessionContext = createContext();

//sessione per la gestione dei login nel app
export const SessionProvider = ({ children }) => {
  const [session, setSession] = useState({

    userSession: {
      username: '',
      isLoggedIn: false,
      isAdmin: false,

      login: function () {
        this.isLoggedIn = true;
      },

      loginA: function(){
          this.isLoggedIn= true;
          this.isAdmin= true;
      },

      logout: function () {
        this.isLoggedIn = false;
      },
    },
  });

  const updateSession = (newSession) => {
    setSession((prevSession) => ({ ...prevSession, ...newSession }));
  };

  return (
    <SessionContext.Provider value={{ session, updateSession }}>
      {children}
    </SessionContext.Provider>
  );
};

const styles = StyleSheet.create({});