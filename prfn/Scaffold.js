import { StyleSheet, Text, View, Button , ScrollView , TouchableOpacity} from 'react-native';
import Ionicons from '@expo/vector-icons/Ionicons';
import Card from './Card.js';
import CustomScrollView from './Card.js';
import { useHistory } from 'react-router-dom'
import { useRoute } from '@react-navigation/native';
import React from 'react';
import { SessionProvider, SessionContext } from './Sessioni.js';
import  { useContext, useEffect , useState }  from 'react';



function Scaffold ({navigation,children}) {
   const route=useRoute();
const { session, updateSession } = useContext(SessionContext);

function prenotazioni(){
   if(session.userSession.isAdmin){
       navigation.replace('Pannelloctr');
   }else
   {
     if(session.userSession.isLoggedIn){
       navigation.replace('Prenotazioni');
     }
   }
}

function tornahome(){
     navigation.replace('Home');
}

 function logout(){
   session.userSession.logout();
   if(route.name=='Home'){
          navigation.replace('Home');
   }
   navigation.navigate('Home');
 }


  return(
    <View style={styles.display}>
       <View style={styles.top}>
          <Text style={styles.title}>My Hotel</Text>
          { session.userSession.isLoggedIn ? (
            <View style={{flexDirection:'column',gap:5}}>
              <Button title='Logout'  onPress={() => logout()}/>
            </View>
          ):(
             <View style={{flexDirection:'column', gap:5}}>
                 <Button title='Login'  onPress={() => navigation.navigate('Login')} />
                  <Button title='Registrati' onPress={()=> navigation.navigate('Registrati')} />
             </View>
          )}
       </View>


       <View style={styles.center}>
        {children}
       </View>


       <View style={styles.bottom}>
       <TouchableOpacity style={styles.botton} onPress={() => prenotazioni()} >
                <Ionicons name="person" size={32} color="black" />
                 </TouchableOpacity>

                 <TouchableOpacity style={styles.botton} onPress={() => {}}>
                          <Ionicons name="information-circle" size={32} color="black" />
                           </TouchableOpacity>

                           <TouchableOpacity style={styles.botton} onPress={() => tornahome()}>
                                    <Ionicons name="home" size={32} color="black" />
                                     </TouchableOpacity>
       </View>
    </View>
  );
}

export default Scaffold;

const styles = StyleSheet.create({
    display:{
    flex:1,
    flexDirection: 'column',
    },
    top:{
      height: 100,
      backgroundColor:'rgba(0, 255, 255, 1)',
      flexDirection: 'row',
      justifyContent: 'space-between',
      alignItems: 'center',
    },
    center:{
      flex:3,
      alignItems: 'center',
      backgroundColor:'rgba(255, 209, 146, 1)',
    },
    bottom:{
      height: 50,
      justifyContent: 'space-between',
      alignItems: 'center',
      flexDirection: 'row',
      backgroundColor:'rgba(0,0,0,0)',
    },

      botton:{
      marginHorizontal: 10, // Riduci la distanza orizzontale tra le icone
          marginLeft: 40, // Aggiungi uno spazio sinistro
          marginRight: 40,

          },
     title:{
          color: 'white',
          fontSize: 24,
          fontWeight: 'bold',
          fontStyle:'italic',
          letterSpacing: 1.5,
          },
          botton2:{
          width: 100,
              backgroundColor: 'rgba(0, 255, 255, 1)',
              paddingVertical: 10,
              paddingHorizontal: 15,
              borderRadius: 5,
          }
})