import React from 'react';
import { View, Text, ScrollView, TouchableOpacity, StyleSheet } from 'react-native';
import { useHistory } from 'react-router-dom'
import { useRoute } from '@react-navigation/native';
import { Image,Alert } from 'react-native';
import { SessionProvider, SessionContext } from './Sessioni.js';
import  { useContext, useEffect , useState }  from 'react';


function Card({ navigation,stanza,prenota }) {

const { session, updateSession } = useContext(SessionContext);

  return (
    <View style={styles.card}>
      <View style={styles.imageContainer}>
        <Image source={{ uri: stanza.Image }} style={styles.image} resizeMode="cover" />
      </View>
      <View style={styles.content}>
      { !prenota ? (
      <>
        <Text style={styles.subtitle}>Servizi: {stanza.Servizi}</Text>
                         <Text style={styles.subtitle}>Numero posti: {stanza.Nposti}</Text>
                         <Text style={styles.subtitle}>Prezzo per notte: {stanza.Prezzo}€</Text>
                        <TouchableOpacity onPress={() => navigation.navigate('Prenota', {stanza: stanza}) } disabled={!session.userSession.isLoggedIn} style={styles.button} >
                                            <Text style={styles.buttonText}>Prenota</Text>
                                          </TouchableOpacity>
                                          <Text style={{ fontSize: 10 }}>*ricorda devi loggarti per prenotare</Text>
      </>
      ):
      (
      <>
       <Text style={styles.subtitle}>Servizi: {stanza.Servizi}</Text>
                               <Text style={styles.subtitle}>Numero posti: {stanza.Nposti}</Text>
                               <Text style={styles.subtitle}>Prezzo per notte: {stanza.Prezzo}€</Text>
      </>
      )
      }
      </View>
    </View>
  );
}
const styles = StyleSheet.create({
  container: {
    gap: 10,
    paddingVertical: 20,
    paddingHorizontal: 10,
  },
  card: {
    width: 350,
    padding: 10,
    marginBottom: 10,
    borderWidth: 2,
    borderColor: 'white',
    backgroundColor: 'white',
    borderRadius: 10,
    flexDirection: 'row',
    alignItems: 'center',
    marginBottom: 10,
  },
  imageContainer: {
    borderColor: 'black',
    borderRadius: 10,
    overflow: 'hidden',
  },
  image: {
    width: 150,
    height: 150,
  },
  content: {
    flex: 1,
    marginLeft: 20,
    paddingVertical: 10,
  },
  subtitle: {
    marginBottom: 5,
  },
  button: {
    width: 100,
    backgroundColor: 'rgba(0, 255, 255, 1)',
    paddingVertical: 10,
    paddingHorizontal: 15,
    borderRadius: 5,
    alignItems: 'center',
  },
  buttonText: {
    color: 'white',
    fontWeight: 'bold',
  },
});

export default Card;