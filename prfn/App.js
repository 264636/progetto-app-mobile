import { StyleSheet, Text, View, Button , ScrollView } from 'react-native';
import React, { useContext, useEffect , useState } from 'react';
import { SessionProvider, SessionContext } from './Sessioni.js';
import {AppLoading} from 'expo';
import * as SQLite from 'expo-sqlite';
import Scaffold from './Scaffold';
import Card from './Card.js';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import {Databasehelper} from './Databasehelper.js';


async function myFunction() {
  const databaseHelper = new Databasehelper();
  const db= SQLite.openDatabase('Myhotel.db');
  await databaseHelper.inizializzaDatabase(db);
}

async function caricastanze(stanza){
  const databaseHelper = new Databasehelper();
  const db= SQLite.openDatabase('Myhotel.db');

  stanza=await databaseHelper.getAllStanza(db);
  setStanze(stanza);
}




export function HomePage({ navigation }) {

[stanze, setStanze] = useState([]);
useEffect(() => {
       caricastanze(setStanze);
     }, []);



const { session, updateSession } = useContext(SessionContext);


  return (
    <Scaffold navigation={navigation} >
    <ScrollView vertical showsVerticalScrollIndicator={false}>
                         {stanze.map((item, index) => (
                           <View key={index} style={styles.menuItem}>
                             <Card stanza={item} navigation={navigation} prenota={false} />
                           </View>
                         ))}
                       </ScrollView>
   </Scaffold>
  );
}

 function App() {
 myFunction();

  const Stack = createStackNavigator();
  return (
   <SessionProvider>
    <NavigationContainer>
      <Stack.Navigator>
        <Stack.Screen name="Home" component={HomePage} options={{ headerShown: false }}/>
        </Stack.Navigator>
    </NavigationContainer>
    </SessionProvider>
  );
}

export default App;

 const styles = StyleSheet.create({});
